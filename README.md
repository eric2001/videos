# Videos

## Description
Videos is a plugin for [Gallery 3](http://gallery.menalto.com/) which allows users to upload "unsupported" video files to their Gallery 3 albums.  By default, this module will allow admins to add videos with the following file extensions to their albums: "avi", "mpg", "mpeg", "mov", "wmv", "asf", "mts". This list of "accepted" file extensions is controlled by a module variable, which can be edited from the Settings -> Advanced screen (it's the "videos" / "allowed_extensions" value).

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
This installs like any other Gallery 3 module.  Download and extract it into your modules folder.  Afterwards log into the Gallery web interface and enable in under the Admin -> Modules menu.  Admin users will then be able to add videos by selecting Add -> Add videos from the album menu.  There is nothing else to configure with this module.

## History
**Version 2.0.0:**
> - Complete re-write for Gallery 3.0.1 compatibility. 
> - Released 22 April 2011.
>
> Download: [Version 2.0.0](/uploads/7f60a6a61c04d6555138b9938bac9e77/videos200.zip)

**Version 1.1.1:**
> - I've implemented Ticket #1154 ( http://sourceforge.net/apps/trac/gallery/ticket/1154 ), which is a fix for playing video's at the correct aspect ratio when flowplayer is in fullscreen mode.
> - Released 06 November 2010.
>
> Download: [Version 1.1.1](/uploads/8771b5267caaf18572b3cfcd123c96e1/videos111.zip)

**Version 1.1.0:**
> - Added support for a flash formatted, "resized" version of the video for playback in the web browser.
> - Released 29 September 2010.
>
> Download: [Version 1.1.0](/uploads/7a4c0339669d4f7e7ce495cf6d6b9a60/videos110.zip)

**Version 1.0.0:**
> - Initial Release.
> - Released on 24 September 2010.
>
> Download: [Version 1.0.0](/uploads/887b241c3e84c7e4d6e3e2d86a5c5fbf/videos100.zip)
